﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class TestCalculator
    {
        public int sum(List<int> list)
        {
            int sum = list.Sum();
            return sum;
        }
        public int returnAbsSum(List<int> listToSum)
        {
            int sum = listToSum.Sum();
            sum = Math.Abs(sum);
            return sum;
        }

        public decimal Divide(int x, int y)
        {
            decimal res;
            try
            {
                res = x / y;
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Nie wolno dzielić przez 0");
                throw;
            }
            return res;
        }

        public List<char> getAllSigns()
        {
            List<char> listOfSigns = new List<char>() { '+', '-', '*', '/' };
            return listOfSigns;
        }


    }
}
