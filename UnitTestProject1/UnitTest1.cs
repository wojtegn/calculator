﻿using Calculator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Assert = NUnit.Framework.Assert;

namespace UnitTestProject1
{
    [TestFixture]
    public class UnitTest1
    {
        TestCalculator _calculator;

        [Test]
        [TestCase(1, 1, 1, 1, 4)]
        [TestCase(1, 1, 1, 0, 3)]
        [TestCase(1, 0, 2, 0, 3)]
        [TestCase(1, 5, 2, 11, 19)]
        [TestCase(0, 0, 0, 0, 0)]
        public void TestCalculator_ShouldSumArray_Correctly(int x, int x1, int x2, int x3, int expected)
        {

            //act
            int res = _calculator.sum(new List<int>() { x, x1, x2, x3 });
            //assert
            Assert.AreEqual(expected, res);

        }
        [Test]
        [TestCase(1, 1, 1, 1, 4)]
        [TestCase(1, 1, 1, 0, 3)]
        [TestCase(1, 0, 2, 0, 3)]
        [TestCase(1, 5, 2, 11, 19)]
        [TestCase(-1, -1, -1, -1, 4)]
        public void TestCalculator_ShouldReturnGreaterThanZero_Correctly(int x, int x1, int x2, int x3, int expected)
        {

            //act
            int res = _calculator.returnAbsSum(new List<int>() { x, x1, x2, x3 });
            //assert
            Assert.Greater(res, 0);

        }

        [Test]
        public void TestCalculator_ShouldReturnDivideByZeroExeption()
        {
            //act
            Assert.Throws<DivideByZeroException>(() => _calculator.Divide(1, 0));

        }

        [Test]
        public void TestCalculator_ShouldReturnListIsNotNull_Correctly()
        {
            //Act
            List<char> list = _calculator.getAllSigns();
            //Assert
            Assert.IsNotNull(list);
        }
        [Test]
        public void TestCalculator_ShouldReturnListContains_Correctly()
        {
            //Act
            List<char> list = _calculator.getAllSigns();
            //Assert
            Assert.IsTrue(list.Contains('+'));
        }


        [SetUp]
        public void Initialize()
        {
            _calculator = new TestCalculator();
        }

    }
}
